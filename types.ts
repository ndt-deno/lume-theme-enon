import { PaginationInfo } from "lume/plugins/paginate.ts"

declare global
{
    namespace Lume.Enon
    {
        export type ContentType = 'page' | 'entry' | 'list'
        export type Theme = 'light' | 'dark' | 'coffee'

        export interface Data extends Lume.Data
        {
            // System
            site_version: string
            type?: ContentType
            default_theme: Theme
            site_title_divider?: string
            banner?: MaybeFunction<string, Data>
            footer?: MaybeFunction<string, Data>
            mainmenu?: MainMenuItem[]
            stylesheet: `/${string}.css`[]
            module_script: `/${string}.js`[]

            // Site Info
            site_name?: string
            site_desc?: string
            site_footer_placeholder?: string
            brand?: Brand
            network?: SocialNetwork[]
            photo_fallback?: PhotoFallback & PathOrLink

            // Page Info
            order: number
            desc?: string
            description?: string
            excerpt?: string
            photo_thumbnail?: PathOrLink
            photo_opengraph?: PathOrLink
        }

        export type Link = `${'http' | 'https'}://${string}.${string}`
        export type LinkRel = 'alternate' | 'author' | 'bookmark' | 'enclosure' | 'external' | 'help' | 'license' | 'next' | 'nofollow' | 'noreferrer' | 'noopener' | 'prev' | 'search' | 'tag'

        export type Path = `${'' | '.' | '..'}/${string}`

        export type PathOrLink = Path | Link

        export interface Brand
        {
            path: string,
            name: string
        }

        export interface MainMenuItem
        {
            name: string,
            url: `/${string}` | `mailto:${string}` | Link,
            target?: '_self' | '_blank' | '_parent' | '_top'
            rel?: LinkRel
        }

        export interface SocialNetwork
        {
            name: string
            link: Link
        }

        export interface BaseHtmlClass
        {
            body?: string[]
        }

        export interface PhotoFallback
        {
            photo_thumbnail?: PathOrLink
            photo_opengraph?: PathOrLink
        }

        /**
         * @see https://github.com/microsoft/TypeScript/issues/37663#issuecomment-605614970
         */
        export type MaybeFunction<V, P> = V extends unknown ? ((p: P) => V) : V
    }

    /**
     * Layout: List data
     */
    namespace Lume.Enon.List
    {
        export interface Data extends Lume.Enon.Data
        {
            list_heading?: string

            more_btn?: {
                label: string,
                url: `/${string}` | Lume.Enon.Link
            }

            pagination: PagiInfo
        }
        export interface PagiInfo extends PaginationInfo
        {
            url_pattern: string
        }
    }

    /**
     * Plugin compatibility between Preact & Inline
     */
    namespace preact.JSX
    {
        export interface HTMLAttributes
        {
            inline?: boolean | undefined
        }
    }
}
