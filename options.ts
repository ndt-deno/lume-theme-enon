import { Options as MultilanguageOptions } from 'lume/plugins/multilanguage.ts'
import { Options as FluentOptions } from 'plugins/fluent'
import { Options as SassOptions } from 'lume/plugins/sass.ts'
import { Options as EsbuildOptions } from 'lume/plugins/esbuild.ts'
import { Options as DateOptions } from 'lume/plugins/date.ts'
import { Options as TransformImagesOptions } from 'lume/plugins/transform_images.ts'
import { Options as InlineOptions } from 'lume/plugins/inline.ts'
import { Options as JsxOptions } from 'lume/plugins/jsx_preact.ts'

export default interface Options
{
    multilanguage: MultilanguageOptions
    fluent: Partial<FluentOptions>
    sass: SassOptions
    esbuild?: EsbuildOptions
    transform_images?: TransformImagesOptions
    date: DateOptions
    inline?: InlineOptions
    jsx?: JsxOptions
}

export const default_options: Options = {
    multilanguage: {
        languages: ['en']
    },
    fluent: {
        fallbackLanguage: 'en',
    },
    sass: {
        format: 'compressed'
    },
    esbuild: {
        extensions: ['.ts', '.js'],
        options: {
            target: 'es2020',
            format: 'esm', bundle: false,
            treeShaking: true,
            minify: true, keepNames: true, sourcemap: true,
        },
    },
    transform_images: undefined,
    date: {
        formats: {
            DATE: 'dd-MM-yyyy'
        }
    },
    inline: undefined,
    jsx: undefined,
}