import lumeCMS from 'lume/cms/mod.ts'

const cms = lumeCMS({
    basePath: '/xyz'
})

export default cms
