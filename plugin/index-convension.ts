/**
 * Idea » https://start.solidjs.com/core-concepts/routing#renaming-index
 * Issue » https://github.com/lumeland/lume/issues/605
 * Solution » https://github.com/lumeland/lume/issues/605#issuecomment-2102892708
 * 
 * UNIX sorting order
 * 1. (abc) ← _data
 * 2. [abc] ← index
 * 3. _abc ← _data
 * 4. abc ← everything else
 */

import { Entry } from 'lume/core/fs.ts'


function is_data(file: Entry): boolean
{
    const [, ext] = file.name.match(/^\(.+\)\.([\w\.]+)$/) ?? []
    if (!ext) return false

    file.name = `_data.${ext}`
    return true
}

function is_index(file: Entry)
{
    const [, ext] = file.name.match(/^\[.+\]\.([\w\.]+)$/) ?? []
    if (!ext) return false

    file.name = `index.${ext}`
    return true
}

export default (site: Lume.Site) =>
{
    site.addEventListener('afterLoad', () =>
    {
        for (const file of site.fs.entries.values())
        {
            if (is_index(file)) continue
            is_data(file)
        }
    })
}