export default (site: Lume.Site) =>
{
    site.mergeKey('stylesheet', 'stringArray')
    site.mergeKey('module_script', 'stringArray')
}