import Site from 'lume/core/site.ts'
import { Page } from 'lume/core/file.ts'
import { log } from 'lume/core/utils/log.ts'

import text_loader from 'lume/core/loaders/text.ts'

async function critical_css(page: Page, site: Site)
{
    const { url } = page.data
    if (!url) return void 0
    if (!url.endsWith('critical.css')) return void 0

    const content = await site.getContent('critical.css', text_loader)
    if (!content) return void log.warn('Missing critical.css file!')

    const content_KB = Math.round(content.length / 1024 * 100) / 100
    content_KB > 13.9 && log.warn(`
        ⚠️ The size of Critical CSS has gone over its limit 14KB!
        Learn more: https://web.dev/articles/extract-critical-css#14KB
    `)

    log.info(`🏋️ critical.css costs ~${content_KB}KB`)
}

export default (site: Lume.Site) =>
{
    // Check if critical CSS reach the limit
    site.process(['.css'], (page_ls) =>
    {
        for (const page of page_ls) return critical_css(page, site)
    })
}