import Options, { default_options } from './options.ts'

import { merge } from 'lume/core/utils/object.ts'

import theme_configuration from './plugin/theme-cfg.ts'
import index_convension from './plugin/index-convension.ts'
import critical_style from './plugin/critical-style.ts'

// import mdx from 'lume/plugins/mdx.ts'
// import multilanguage from 'lume/plugins/multilanguage.ts'
// import fluent from 'plugins/fluent'
import sass from 'lume/plugins/sass.ts'
import esbuild from 'lume/plugins/esbuild.ts'
import transform_images from 'lume/plugins/transform_images.ts'
import date from 'lume/plugins/date.ts'
import inline from 'lume/plugins/inline.ts'
import jsx from 'lume/plugins/jsx_preact.ts'

export default function (user_options?: Partial<Options>)
{
    const options = merge(default_options, user_options)

    return (site: Lume.Site) => load_plugin(options, site)
}

function load_plugin(options: Options, site: Lume.Site)
{
    site.use(theme_configuration)
    site.use(index_convension)

    site.use(sass(options.sass))
    site.use(esbuild(options.esbuild))
    site.use(transform_images(options.transform_images))
    site.use(date(options.date))
    site.use(inline(options.inline))
    site.use(jsx(options.jsx))

    site.use(critical_style)

    // site.use(multilanguage(options.multilanguage))
    // site.use(fluent(options.fluent))
}