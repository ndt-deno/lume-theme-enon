import lume from 'lume/mod.ts'

import plugin_set from './plugin.ts'

const site = lume({
    src: './src',
    includes: './_layout',
    dest: './public'
})

site.use(plugin_set())

export default site
