import { PaginateOptions } from 'lume/plugins/paginate.ts'

export const layout = 'list.tsx'

// List Layout Data only
export const list_heading = 'Recent Entries'

const query = {
    params: 'type=entry',
    sort_by: 'date=desc',
    url: (n: number) => n === 1 ? `/entry/` : `/entry/${n}/`,
    size: 2, // TODO 10
}

export default function* (data: Lume.Enon.Data, _helpers: Lume.Helpers)
{
    const pages = data.search.pages<Lume.Enon.List.Data>(query.params, query.sort_by)

    const options: Partial<PaginateOptions> = {
        url: query.url,
        size: query.size,
    }

    for (const page of data.paginate(pages, options))
    {
        (page.pagination as Lume.Enon.List.PagiInfo).url_pattern = '/entry/{n}'
        yield page
    }
}

