import { Transformation } from "lume/plugins/transform_images.ts"

export const transformImages: Transformation[] = [
    {
        resize: [],
        format: ['webp', 'jpeg']
    },
    {
        // 
    }
]