export const site_version = 'dev'
export const site_name = 'Demo Theme'
export const site_desc = "How do you tell one site uses modern technology? Try it out in Safari. You'll know it's a modern once it ends up crashing & burning."

export const mainmenu: Lume.Enon.MainMenuItem[] = [
    {
        name: 'Home',
        url: '/'
    },
    {
        name: 'Entries',
        url: '/entry/'
    },
    {
        name: 'Get started',
        url: '/entry/get-started/'
    },
    {
        name: 'About',
        url: '/about/'
    },
    {
        name: 'Author',
        url: 'https://cv.ngdangtu.com',
        rel: 'author'
    },
]

export const network: Lume.Enon.SocialNetwork[] = [
    {
        name: 'gitlab',
        link: 'https://gitlab.com/ngdangtu',
    },
    {
        name: 'linkedin',
        link: 'https://www.linkedin.com/in/ngdangtu',
    },
    {
        name: 'github',
        link: 'https://github.com/ngdangtu-vn',
    },
]

export const footer = (_: Lume.Enon.Data) => /*html*/`
    <blockquote cite='https://www.azquotes.com/quote/666655'>
        <p>Software is like sex: It's better when it's free.</p>
        <footer>— <cite>Attributed to Torvalds at 1996 FSF conference</cite></footer>
    </blockquote>
`

export const photo_fallback: Lume.Enon.PhotoFallback = {
    photo_thumbnail: '/res/fallback-thumbnail.webp',    // https://www.shopify.com/stock-photos/photos/small-black-and-white-bird-with-a-yellow-belly
    photo_opengraph: '/res/fallback-opengraph.webp',    // https://www.shopify.com/stock-photos/photos/small-black-and-white-bird-with-a-yellow-belly
}

export const site_title_divider = '»' // | « » ∈ : ∴

// TODO remove on 2.2.2
export const stylesheet = '/base.css'
export const module_script = '/frontend/base.js'
