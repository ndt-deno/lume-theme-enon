import Article from "./module/list/article.tsx";
import Pagination from "./module/list/pagination.tsx";


export const type = 'list'
export const layout = '_base.tsx'

export const stylesheet = '/list.css'


export default function (d: Lume.Enon.List.Data, helpers: Lume.Helpers)
{
    if (!d.results) return <></>

    const results = d.results as Lume.Enon.Data[]

    return (<>
        <article id='maincontent' class='container'>
            {d.list_heading && <h2>{d.list_heading}</h2>}
            ${results.map(result => <Article data={result} helpers={helpers} />)}
        </article>
        {d.more_btn && (
            <aside>
                <a role='button' href={d.more_btn.url}>{d.more_btn.label}</a>
            </aside>
        )}
        <Pagination info={d.pagination} />
    </>)
}

