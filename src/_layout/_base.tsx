import { maybefn } from './module/utility.ts'

export const site_title_divider = '∈'
export const default_theme = 'light'
export const stylesheet = '/base.css'
export const module_script = '/frontend/base.js' // FIXME https://github.com/lumeland/lume/issues/617

export default (d: Lume.Enon.Data, _helpers: Lume.Helpers) =>
{
    const title = d.title ? `${d.title} ${d.site_title_divider} ${d.site_name}` : d.site_name
    const desc = d.desc ?? d.description ?? d.site_desc
    const brand = d.brand ? <img class='logo' alt={d.brand.name} src={d.brand.path} /> : d.site_name
    
    const banner = maybefn(d.banner, d)
    const footer = maybefn(d.footer, d)

    return (
    <html lang={d.lang} data-version={d.site_version}>
    <head>
        <meta charset='UTF-8'/>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'/>

        <title>{title}</title>
        {desc && <meta name='description' content={desc}/>}

        <link inline rel='stylesheet' href='/critical.css' />
    </head>
    <body data-theme={d.default_theme}>
        {d.stylesheet?.map(src => <link rel='stylesheet' href={src} />)}
        
        <header class='page-header wrapper-top'>
            <div class='container'>
                <a class='brand'>{brand}</a>
                {d.mainmenu && (
                    <nav class='main-menu'>
                        {d.mainmenu?.map(({name, url, ...rest}) =>(
                            <a class='item' href={url} {...rest}>
                                {name}
                            </a>
                        ))}
                    </nav>
                )}
            </div>
        </header>
        
        {banner && <aside class='banner container' dangerouslySetInnerHTML={{__html: banner}} />}
        
        <main class='page-body wrapper-top' dangerouslySetInnerHTML={{__html: d.content as string}}/>
        
        <footer class='page-footer wrapper-top'>
            <menu class='container'>
                <li>
                    <button data-ico-start='light' data-action='apply-theme' data-theme='light'>light</button>
                </li>
                <li>
                    <button data-ico-end='dark' data-action='apply-theme' data-theme='dark'>dark</button>
                </li>
            </menu>
            {d.network && (
                <ul class='social-network'>
                    {d.network?.map(({ name, link }) => (
                        <li>
                            <a title={name} data-ico-start={name} href={link}/>
                        </li>
                    ))}
                </ul>
            )}
            {footer && <div class='container' dangerouslySetInnerHTML={{__html: footer}} />}
        </footer>

        <button id='top'>go top</button>
        {d.module_script?.map(src => <script type='module' src={src}/>)}        
    </body>
    </html>)
}
    

