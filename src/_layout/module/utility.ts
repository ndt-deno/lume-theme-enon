/**
 * Conditional Tag
 */
export const iftag = <T = unknown, R = unknown>(
    maybe_val: T | undefined | null,
    has_val: string | ((this: R | undefined, v: T) => string),
    no_val: string = '',
    this_arg: R | undefined = undefined,
): string =>
{
    if (!maybe_val) return no_val
    if (typeof has_val === 'string') return has_val

    return has_val.call(this_arg, maybe_val)
}

/**
 * Loop Tag
 */
export const fortag = <T = unknown, R = unknown>(
    list: T[] | undefined | null,
    items: (this: R | undefined, item: T, index: number) => string,
    divider: string = '',
    empty: string = '',
    this_arg: R | undefined = undefined,
): string =>
{
    if (!list) return empty

    return list
        .map((v, i) => items.bind(this_arg, v, i))
        .join(divider)
}

/**
 * HTML Attributes
 */
export const attr = (kvlist: Record<string, unknown>): string =>
{
    let result = ''
    for (const key in kvlist)
    {
        if (!Object.prototype.hasOwnProperty.call(kvlist, key)) continue
        const value = kvlist[key]
        if (value === false) continue
        if (Array.isArray(value))
        {
            result += `${key}="${value.filter(v => v).join(' ')}"`
            continue
        }
        result += value === true ? key : `${key}="${value}"`
    }
    return result
}

/**
 * Get data that could be function type
 */
export const maybefn = <V, P>(maybe?: Lume.Enon.MaybeFunction<V, P>, fn_param?: P) =>
{
    if (!maybe) return void 0

    if (typeof maybe === 'function')
    {
        return maybe(fn_param)
    }

    return maybe
}

/**
 * Photo Fallback
 * @see Lume.Enon.PhotoFallback
 */
export const photo_fallback = (key: string, d: Lume.Enon.Data): string =>
{
    if (d[key]) return d[key]

    const fallback = d.photo_fallback
    if (!fallback) return ''

    if (typeof fallback === 'string') return fallback
    return fallback[key] || ''
}