import { photo_fallback } from "../utility.ts";

interface ArticleProp {
    data: Lume.Enon.Data,
    helpers: Lume.Helpers
}

export default function Article(prop: ArticleProp)
{
    const { data, helpers } = prop
    const thumbnail = photo_fallback('photo_thumbnail', data)

    return (
        <article class='excerpt'>
            <a href={data.url}>
                {thumbnail && <img src={thumbnail}/>}
                <time title='created date' datetime={data.date.toISOString()}>
                    {helpers.date(data.date, 'DATE')}
                </time>
                <h3 class='clamp-line-2'>{data.title}</h3>
                {data.topic && <small class='clamp-line-2'>{data.topic}</small>}
                {data.excerpt && <p class='excerpt'>{data.excerpt}</p>}
            </a>
        </article>
    )
}