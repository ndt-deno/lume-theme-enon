interface PaginationProp {
    info?: Lume.Enon.List.PagiInfo
}

const default_info: Partial<Lume.Enon.List.PagiInfo> = {
    next: null,
    previous: null,
}

export default function Pagination(prop: PaginationProp)
{
    if (!prop.info) return <></>

    const { next, page, previous, totalPages, totalResults, url_pattern } = {
        ...default_info,
        ...prop.info,
    }

    if (totalResults < 1) return <></>
    if (totalPages < 1) return <></>

    // const url_pattern = (next || previous)?.split('/')
    if (!url_pattern || url_pattern.length === 0) return <></>

    // TODO it will be broken if pattern has number in other position
    const ln = (n: number) => url_pattern.replace('{n}', n > 1 ? n.toString(10) : '')

    return (
        <nav class='pagi' data-pattern='${url_pattern}'>
            {
                page > 2
                    ? <a title='go to first page' href={ln(1)}>First</a>
                    : <span/>
            }
            {
                previous
                    ? <a title='go to previous page' href={previous}>Previous</a>
                    : <span/>
            }
            <button title='go to a specific page (open a prompt)'>
                {page} 
            </button>
            <span title='total pages'>
                {totalPages}
            </span>
            {
                next
                    ? <a title='go to next page' href={next}>Next</a>
                    : <span/>
            }
            {
                page < totalPages - 1
                    ? <a title='go to last page' href={ln(totalPages)}>Last</a>
                    : <span/>
            }
        </nav>
    )
}