import { iftag, attr } from "./module/utility.ts"

export const type = 'entry'
export const layout = '_base.tsx'

export const stylesheet = '/lab-ui.css'

export default () => (<>
    <nav class='xxx'>
        <strong>Change theme</strong>
        <menu>
            <li>light</li>
            <li>dark</li>
        </menu>
    </nav>
    <div class='container'>
        <section class='test-room'>
            <h2 class='test-desc'>§ Color Palette</h2>

            <ul>
                <li>Brand</li>
                <li>Accent</li>
            </ul>
        </section>
        <section class='test-room'>
            <h2 class='test-desc'>Components</h2>

            <h3>Button</h3>
            <div>
                <button class='sm'>plain button</button>
                <button class='sub sm'>sub button</button>
                <button class='main sm'>main button</button>
                
                <button class='md'>plain button</button>
                <button class='sub md'>sub button</button>
                <button class='main md'>main button</button>
                
                <button class='lg'>plain button</button>
                <button class='sub lg'>sub button</button>
                <button class='main lg'>main button</button>
            </div>

            <h3>Inline</h3>
            <div>
                <p>Suspendisse <a href=''>potenti</a>. Suspendisse id tempus erat. <b>Suspendisse potenti</b>. Nulla eleifend eros et <em>velit faucibus ultrices</em>. Etiam sit <u>amet nibh finibus</u>, <i>sodales augue ac, aliquet lacus</i>. Ut sit amet <code>sapien tincidunt</code>, auctor libero vitae, scelerisque eros. <mark>Vivamus a lacus auctor, volutpat risus vel</mark>, tempor augue. Phasellus malesuada nibh eget est consequat: <code>result += `$&#123;key&#125;="$&#123;value&#125;.filter(v =&gt; v).join(' ')"`</code>. <cite>In sagittis ipsum efficitur</cite>. Integer consectetur, tellus non aliquet commodo, lacus lorem posuere est, in hendrerit sapien urna a sapien. Quisque ut mollis quam. <dfn id='dfn-ndt'><abbr title="Nguyễn Đăng Tú">NDT</abbr></dfn>!</p>
                <p><dfn id='dfn-http'>Hypertext Transfer Protocol</dfn> (<abbr>HTTP</abbr>)? Nulla <s>eleifend eros et</s> velit faucibus ultrices. Etiam sit amet nibh finibus, sodales augue ac, aliquet lacus. Ut sit amet sapien tincidunt, auctor libero vitae, scelerisque eros. Vivamus a lacus auctor, volutpat risus vel, tempor augue. Phasellus malesuada nibh eget est consequat, in sagittis ipsum efficitur. Integer consectetur, tellus non aliquet commodo, lacus lorem posuere est, in hendrerit sapien urna a sapien. Quisque ut mollis quam.</p>
            </div>

            <h3>Other</h3>
            <div>
                <figure>
                    <img src='/res/big-photo.webp'/>
                    <figcaption>
                        <p>credit: <cite><a href='https://unsplash.com/photos/person-showing-thumb-4Vg6ez9jaec'>Katya Ross</a></cite></p>
                    </figcaption>
                </figure>
                <blockquote>
                    <p>People ignore designs that ignore people.</p>
                    <footer>— <cite>Frank Chimero</cite></footer>
                </blockquote>
            </div>
        </section>
        <section class='test-room'>
            <h2 class='test-desc'>Typography</h2>

            <h1>Lorem ipsum dolor sit amet</h1>
            <h2>Lorem ipsum dolor sit amet</h2>
            <h3>Lorem ipsum dolor sit amet</h3>
            <h4>Lorem ipsum dolor sit amet</h4>
            <h5>Lorem ipsum dolor sit amet</h5>
            <h6>Lorem ipsum dolor sit amet</h6>
            <p>Suspendisse potenti. Suspendisse id tempus erat. Suspendisse potenti. Nulla eleifend eros et velit faucibus ultrices.</p>
        </section>
        <section class='test-room'>
            <h2 class='test-desc'>Markdown Style</h2>
            <p><a href='https://web.dev/articles/building/a-color-scheme'>https://web.dev/articles/building/a-color-scheme</a></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et rutrum tellus. Morbi ligula urna, venenatis vel viverra vitae, condimentum a libero. Vestibulum porttitor placerat velit ac ullamcorper. Etiam dapibus libero tellus, eu molestie dolor accumsan sed. Praesent dapibus ullamcorper hendrerit. In mattis, nibh sed pretium aliquam, dolor lectus eleifend nibh, vitae lacinia mauris lorem eget arcu. Nulla dictum euismod elementum.</p>
            <p>Suspendisse potenti. Suspendisse id tempus erat. Suspendisse potenti. Nulla eleifend eros et velit faucibus ultrices. Etiam sit amet nibh finibus, sodales augue ac, aliquet lacus. Ut sit amet sapien tincidunt, auctor libero vitae, scelerisque eros. Vivamus a lacus auctor, volutpat risus vel, tempor augue. Phasellus malesuada nibh eget est consequat, in sagittis ipsum efficitur. Integer consectetur, tellus non aliquet commodo, lacus lorem posuere est, in hendrerit sapien urna a sapien. Quisque ut mollis quam.</p>
        </section>
    </div>
</>)