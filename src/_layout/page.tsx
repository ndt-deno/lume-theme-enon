export const type = 'page'
export const layout = '_base.tsx'

export default function (data: Lume.Data, _helpers: Lume.Helpers)
{
    return (
        <article>
            <div dangerouslySetInnerHTML={{__html: data.content as string || ''}}/>
        </article>
    )
}