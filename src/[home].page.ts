export const layout = 'list.tsx'
export const desc = 'You made it! And we were just about to send out the search party 🎉'

// List Layout Data only
export const list_heading = 'Latest Entries'
export const more_btn = {
    label: 'Read more',
    url: '/entry/',
}

const query = {
    params: 'type=entry',
    sort_by: 'order=desc date=desc',
    size: 5,
}

export default function* (data: Lume.Enon.Data, _helpers: Lume.Helpers)
{
    data.results = data.search.pages(query.params, query.sort_by, query.size)
    yield data
}

