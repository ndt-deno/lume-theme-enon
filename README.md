# Enon

> **Fun fact**:\
> _Enon_ is actually a revesed of _nonE_ (none). None means nothing to
> configure. Simply install it and deploy.

## Specification

### Features

- Great for simple blog
- Optional support multilingual system powered by
  [Fluent](https://projectfluent.org)
- Optional support [GraphComment](https://www.graphcomment.com)
  - a good alternative of Disqus
  - a comment system
- Optional support [GoatCounter](https://goatcounter.com)
  - an ok alternative of Google Analytics
  - an anonymous website tracking system.

### Configuration

See [`options.ts`](./options.ts)

### Requirement

- Deno >= `1.43.5`
- Lume >= `2.2.0`

### Plugin Bundle

- `sass` [official](https://github.com/lumeland/lume/blob/main/plugins/sass.ts)

**TODO**

- `multilanguage`
  [official](https://github.com/lumeland/lume/blob/main/plugins/multilanguage.ts)
- `lume_fluent` [ngdangtu](https://github.com/ngdangtu-vn/mirror-lume-fluent)

## Installation

1. Install it in one of these ways:
   - `deno run -A https://lume.land/init.ts --theme=enon`
   - add this code to your Lume `_config.ts` file:
     ```typescript
     import enon from "https://gitlab.com/ndt-deno/lume-theme-enon/mod.ts";

     site.use(enon());
     ```
2. Set config for theme
