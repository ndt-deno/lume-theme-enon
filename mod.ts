import 'lume/types.ts'

import Options from './options.ts'
import plugin_set from './plugin.ts'

const included_files = [
    '_base.tsx',
    'list.tsx',
    'entry.ts',
    'page.tsx',
]

const data_files = [
    // Default configuration
    '(global).ts',
    'entry/(entry).ts',

    // Default query
    '[home].page.tsx',
    'entry/[entry].page.tsx',

    // CSS files
    'frontend/base.scss',
    'frontend/list.scss',
    'frontend/singular.scss',
    'frontend/style/_cfg.scss',
    'frontend/style/_layer-rule.scss',
    'frontend/style/_markdown.scss',

    // JavaScript client
    'frontend/base.ts',
    'frontend/worker.ts',
    'frontend/script/_pagination.scss',
    'frontend/script/_search.scss',
]

export default function (options: Partial<Options>)
{
    return (site: Lume.Site) =>
    {
        site.use(plugin_set(options))

        for (const file of included_files) site.remoteFile(
            `${site.options.includes ?? '_includes'}/${file}`,
            import.meta.resolve(`./src/_layout/${file}`)
        )

        for (const file of data_files) site.remoteFile(
            file,
            import.meta.resolve(`./src/${file}`)
        )

        site.remoteFile(
            'index.md',
            import.meta.resolve('./src/index.md')
        )
        site.remoteFile(
            '404.md',
            import.meta.resolve('./src/404.md')
        )
    }
}
